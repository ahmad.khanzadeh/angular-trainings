import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-router-body',
  templateUrl: './router-body.component.html',
  styleUrls: ['./router-body.component.scss']
})
export class RouterBodyComponent {
  'dynamicTitle'='getting dynamic informations from component.ts';
  firstName='Ahmad';
  lastName='khanzadeh';
  flag=true;
  divClass='bold-div'

  hide:boolean=false;

  disapear() :void{
    this.hide=!this.hide;
  }

  whatUserTypes=' ';
  inputReciver(f:any){
    this.whatUserTypes=f.value;
    console.log(f);
  }
}

// export class RouterBodyComponent implements OnInit {
//   'dynamic-title'='Ahmad',
//   constructor() { 
   
//   }

//   ngOnInit(): void {
//   }
  

// }
