import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterBodyComponent } from './router-body.component';

describe('RouterBodyComponent', () => {
  let component: RouterBodyComponent;
  let fixture: ComponentFixture<RouterBodyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RouterBodyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterBodyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
